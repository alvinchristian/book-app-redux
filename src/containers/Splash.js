import {StyleSheet, Text, View, StatusBar, Image} from 'react-native';
import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';

const Splash = () => {
  const userData = useSelector(state => state.authData.userData);
  const userToken =
    userData != null
      ? useSelector(state => state.authData.userData.tokens.access.token)
      : null;
  const isLogin = userToken != null ? 'Home' : 'Login';

  const navigation = useNavigation();

  useEffect(() => {
    setTimeout(() => {
      navigation.replace(isLogin);
    }, 2000);
  }, []);

  return (
    <View style={styles.Container}>
      <StatusBar backgroundColor="#F0F4FF" barStyle="dark-content" />
      <Image style={styles.Logo} source={require('../assets/logo.png')} />
      <Text style={styles.MyName}>Alvin Christian</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F0F4FF',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: '40%',
    paddingBottom: '10%',
  },
  Logo: {
    width: 200,
    height: 200,
  },
  MyName: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#8C96B1',
  },
});

export default Splash;
