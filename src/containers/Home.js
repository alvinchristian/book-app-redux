import {
  View,
  Text,
  RefreshControl,
  TouchableOpacity,
  Image,
  StatusBar,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
} from 'react-native';
import React, {useState, useCallback, useEffect} from 'react';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {
  getBooks,
  recBooks,
  clearUserData,
  rupiah,
  clearBooksData,
  connectionChecker,
} from '../actions/Index';
import CircleButton from '../components/CircleButton';
import NoConnection from '../components/NoConnection';

const Home = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const isFocused = useIsFocused();

  const [isLoading, setIsLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const userData = useSelector(state => state.authData.userData);
  const userToken = userData != null ? userData.tokens.access.token : null;
  const userName = userData != null ? userData.user.name : null;
  const firstName = userName != null ? userName.split(' ')[0] : null;

  const BooksData = useSelector(state => state.booksData.booksData);
  const RecBooks = useSelector(state => state.booksData.recBooksData);
  const connection = useSelector(state => state.utilsData.connectionStatus);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setIsLoading(true);
    dispatch(clearBooksData());
    getData();
    setRefreshing(false);
  }, []);

  const getData = () => {
    setIsLoading(true);
    if (BooksData != null && RecBooks != null) {
      setIsLoading(false);
    } else {
      dispatch(getBooks(userToken));
      dispatch(recBooks(userToken));
    }
  };

  useEffect(() => {
    dispatch(connectionChecker());
    if (isFocused) {
      getData();
    }
  }, [connection, isFocused, BooksData, RecBooks]);

  const isLogout = () => {
    navigation.navigate('Login');
    dispatch(clearUserData());
    dispatch(clearBooksData());
  };

  return (
    <View style={styles.Container}>
      <StatusBar barStyle="dark-content" backgroundColor="#F0F4FF" />
      <View style={styles.Header}>
        <Text style={styles.Name}>{`Hello, Welcome ${firstName}!`}</Text>
        <CircleButton Name={'log-out'} onPress={() => isLogout()} />
      </View>
      <View style={styles.MainContainer}>
        {!connection && !RecBooks && !BooksData ? (
          <NoConnection />
        ) : isLoading ? (
          <ActivityIndicator size={50} color="#4267CD" />
        ) : (
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }>
            <Text style={styles.SubTitle}>Recommended</Text>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {RecBooks?.map(item => (
                <View key={item.id} style={styles.RecContainer}>
                  <TouchableOpacity
                    style={styles.Box}
                    onPress={() => {
                      navigation.navigate('Detail', {
                        bookId: item.id,
                      });
                    }}>
                    <Image
                      style={styles.RecCoverImage}
                      source={{uri: item.cover_image}}
                    />
                    <Text style={styles.RecTitle} numberOfLines={2}>
                      {item.title}
                    </Text>
                    <Text style={styles.RecVote}>{item.average_rating}</Text>
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
            <Text style={styles.SubTitle}>Popular</Text>
            {BooksData?.map(item => (
              <View key={item.id} style={styles.LatContainer}>
                <TouchableOpacity
                  style={styles.Card}
                  onPress={() => {
                    navigation.navigate('Detail', {
                      bookId: item.id,
                    });
                  }}>
                  <Image
                    style={styles.LatCoverImage}
                    source={{uri: item.cover_image}}
                  />
                  <View style={styles.Content}>
                    <Text style={styles.Title} numberOfLines={1}>
                      {item.title}
                    </Text>
                    <Text style={styles.Info} numberOfLines={1}>
                      Author : {item.author}
                    </Text>
                    <Text style={styles.Info} numberOfLines={1}>
                      {item.publisher}
                    </Text>
                    <Text style={styles.Info} numberOfLines={1}>
                      Rp. {rupiah(item.price)}
                    </Text>
                  </View>
                  <Text style={styles.Vote}>{item.average_rating}</Text>
                </TouchableOpacity>
              </View>
            ))}
          </ScrollView>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F0F4FF',
  },
  Header: {
    backgroundColor: '#fff',
    paddingHorizontal: 25,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 8,
  },
  Name: {
    color: '#404C6D',
    fontSize: 14,
  },
  MainContainer: {
    flex: 1,
    paddingHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  SubTitle: {
    color: '#404C6D',
    fontSize: 16,
    marginTop: 20,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  RecContainer: {
    width: 140,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#fff',
    paddingBottom: 25,
  },
  Box: {
    width: 120,
    justifyContent: 'center',
    alignItems: 'center',
  },
  RecCoverImage: {
    resizeMode: 'stretch',
    width: 120,
    height: 180,
    borderRadius: 8,
  },
  RecTitle: {
    color: '#404C6D',
    marginTop: 8,
    textAlign: 'center',
    textAlignVertical: 'center',
    lineHeight: 20,
    height: 40,
  },
  RecVote: {
    color: '#404C6D',
    marginTop: 5,
  },
  LatContainer: {
    paddingTop: 20,
    marginBottom: 20,
  },
  Card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    borderRadius: 10,
    paddingHorizontal: 20,
    height: 120,
  },
  LatCoverImage: {
    width: 90,
    height: 120,
    borderRadius: 8,
    resizeMode: 'stretch',
    marginTop: -20,
    marginRight: 10,
  },
  Content: {
    paddingTop: 20,
    paddingBottom: 20,
    justifyContent: 'space-between',
  },
  Title: {
    width: 145,
    color: '#404C6D',
    fontSize: 14,
    fontWeight: 'bold',
  },
  Info: {
    color: '#8C96B1',
    fontSize: 12,
  },
  Vote: {
    color: '#4267CD',
    paddingTop: 20,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Home;
