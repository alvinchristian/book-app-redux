import {
  View,
  Text,
  Image,
  ScrollView,
  StatusBar,
  RefreshControl,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import React, {useState, useEffect, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation, useRoute, useIsFocused} from '@react-navigation/native';
import Share from 'react-native-share';
import {
  clearBooksData,
  getDetailBook,
  connectionChecker,
  rupiah,
} from '../actions/Index';
import CircleButton from '../components/CircleButton';
import {notifikasi} from '../utils/Notifikasi';
import NoConnection from '../components/NoConnection';

const Detail = () => {
  const route = useRoute();
  const isFocused = useIsFocused();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [IsLoading, setIsLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const userData = useSelector(state => state.authData.userData);
  const userToken = userData != null ? userData.tokens.access.token : null;

  const BookDetail = useSelector(state => state.booksData.bookDetail);
  const connection = useSelector(state => state.utilsData.connectionStatus);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setIsLoading(true);
    dispatch(clearBooksData());
    getData();
    setRefreshing(false);
  }, []);

  const onShare = async () => {
    const shareOptions = {
      message: `Movie Title : ${BookDetail.title}\nAuthor : ${BookDetail.author}\nPublisher: ${BookDetail.publisher}\nSynopsis: ${BookDetail.synopsis}`,
    };
    try {
      const ShareResponse = await Share.open(shareOptions);
      console.log(JSON.stringify(ShareResponse));
    } catch (err) {
      console.log(err);
    }
  };

  const getData = () => {
    setIsLoading(true);
    if (BookDetail != null) {
      setIsLoading(false);
    } else {
      dispatch(getDetailBook(userToken, route.params.bookId));
    }
  };

  useEffect(() => {
    dispatch(connectionChecker());
    if (isFocused) {
      getData();
    }
  }, [connection, isFocused, BookDetail]);

  return (
    <View style={styles.Container}>
      <StatusBar backgroundColor="#F0F4FF" barStyle="dark-content" />
      <View style={styles.Header}>
        <CircleButton
          Name={'arrow-left'}
          onPress={() => {
            navigation.goBack();
            dispatch(clearBooksData());
          }}
        />
        <View style={styles.Right}>
          <CircleButton
            Name={'heart'}
            onPress={() => {
              notifikasi.configure();
              notifikasi.buatChannel('1');
              notifikasi.kirimNotifikasi(
                '1',
                'App Notifications',
                `Kamu Menyukai ${BookDetail.title}`,
              );
            }}
          />
          <CircleButton Name={'share'} onPress={onShare} />
        </View>
      </View>
      <View style={styles.MainContainer}>
        {!connection && !BookDetail ? (
          <NoConnection />
        ) : IsLoading ? (
          <ActivityIndicator size={50} color="#4D96FF" />
        ) : (
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }>
            <View style={styles.Card}>
              <Image
                style={styles.CoverImage}
                source={{
                  uri: BookDetail != null ? BookDetail.cover_image : null,
                }}
              />
              <View style={styles.Content}>
                <Text style={styles.Title}>
                  {BookDetail != null ? BookDetail.title : null}
                </Text>
                <Text style={styles.Author}>
                  Author : {BookDetail != null ? BookDetail.author : null}
                </Text>
                <Text style={styles.Publisher}>
                  Publisher : {BookDetail != null ? BookDetail.publisher : null}
                </Text>
              </View>
            </View>
            <View style={styles.Card2}>
              <View style={styles.Box}>
                <Text style={styles.SubTitle}>Rating</Text>
                <Text style={styles.Point}>
                  {BookDetail != null ? BookDetail.average_rating : null}
                </Text>
              </View>
              <View style={styles.Box}>
                <Text style={styles.SubTitle}>Total Sale</Text>
                <Text style={styles.Point}>
                  {BookDetail != null ? BookDetail.total_sale : null}
                </Text>
              </View>
              <TouchableOpacity style={styles.ButtonPrice}>
                <Text style={styles.Price}>
                  {`Buy ${
                    BookDetail != null
                      ? `Rp. ${rupiah(BookDetail.price)}`
                      : null
                  }`}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.Footer}>
              <Text style={styles.Overview}>Overview</Text>
              <Text style={styles.Synopsis}>
                {BookDetail != null ? BookDetail.synopsis : null}
              </Text>
            </View>
          </ScrollView>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F0F4FF',
  },
  Header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    paddingHorizontal: 25,
    backgroundColor: '#fff',
    borderRadius: 8,
  },
  Right: {
    flexDirection: 'row',
    width: 66,
    justifyContent: 'space-between',
  },
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  Card: {
    marginTop: 20,
    paddingHorizontal: 25,
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 8,
    paddingVertical: 25,
  },
  CoverImage: {
    width: 120,
    height: 180,
    resizeMode: 'stretch',
    borderRadius: 8,
  },
  Content: {
    paddingVertical: 40,
    paddingLeft: 20,
    width: 220,
    justifyContent: 'space-between',
  },
  Title: {
    color: '#404C6D',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: 'bold',
  },
  Author: {
    color: '#8C96B1',
    fontSize: 12,
  },
  Publisher: {
    color: '#8C96B1',
    fontSize: 12,
  },
  Card2: {
    marginTop: 20,
    paddingHorizontal: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 25,
  },
  SubTitle: {
    color: '#404C6D',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  Box: {
    height: 42,
    justifyContent: 'space-between',
  },
  Point: {
    color: '#8C96B1',
    fontSize: 14,
    textAlign: 'center',
  },
  ButtonPrice: {
    width: 150,
    backgroundColor: '#4267CD',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  Price: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'bold',
  },
  Footer: {
    backgroundColor: '#fff',
    paddingHorizontal: 25,
    paddingVertical: 25,
    marginVertical: 20,
    borderRadius: 8,
  },
  Overview: {
    color: '#404C6D',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  Synopsis: {
    color: '#8C96B1',
    textAlign: 'justify',
    lineHeight: 20,
  },
});

export default Detail;
