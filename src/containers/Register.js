import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {signUp, clearRegisterStatus, connectionChecker} from '../actions/Index';
import Input from '../components/Input';
import Button from '../components/Button';
import NoConnection from '../components/NoConnection';

const Register = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const isFocused = useIsFocused();

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isSecure, setIsSecure] = useState(true);

  const RegisterStatus = useSelector(state => state.authData.registerStatus);
  const connection = useSelector(state => state.utilsData.connectionStatus);

  useEffect(() => {
    dispatch(connectionChecker());
    if (isFocused) {
      if (RegisterStatus) {
        navigation.navigate('SuccessRegister');
        dispatch(clearRegisterStatus());
      }
    }
  }, [connection, isFocused, RegisterStatus]);

  return (
    <KeyboardAvoidingView
      style={styles.Container}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <StatusBar backgroundColor={'#F0F4FF'} barStyle="dark-content" />
      {!connection ? (
        <NoConnection />
      ) : (
        <View style={styles.MainContainer}>
          <Image style={styles.Logo} source={require('../assets/logo.png')} />
          <View style={styles.InputField}>
            <Input
              placheholder="Fullname"
              onChangeText={value => setName(value)}
            />
            <Input
              placheholder="Email"
              onChangeText={value => setEmail(value)}
            />
            <Input
              placheholder="Password"
              secureTextEntry={isSecure}
              onChangeText={value => setPassword(value)}
            />
            <Button
              text={'REGISTER'}
              onPress={() => dispatch(signUp({name, email, password}))}
            />
          </View>
          <View style={styles.Login}>
            <Text style={styles.Ask}>Already have an Account? </Text>
            <TouchableOpacity>
              <Text
                style={styles.Answer}
                onPress={() => navigation.navigate('Login')}>
                Login
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F0F4FF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  MainContainer: {
    paddingTop: '20%',
    alignItems: 'center',
  },
  Logo: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  InputField: {
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 50,
  },
  Login: {
    alignItems: 'center',
  },
  Ask: {
    marginBottom: 5,
    fontSize: 12,
    color: '#404C6D',
  },
  Answer: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#404C6D',
  },
});

export default Register;
