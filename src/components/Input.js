import {TextInput, StyleSheet} from 'react-native';
import React from 'react';

const Input = ({placheholder, onChangeText, secureTextEntry}) => {
  return (
    <TextInput
      placeholder={placheholder}
      placeholderTextColor="#404C6D"
      style={styles.TextInput}
      onChangeText={onChangeText}
      secureTextEntry={secureTextEntry}
    />
  );
};

const styles = StyleSheet.create({
  TextInput: {
    backgroundColor: '#fff',
    opacity: 0.6,
    height: 60,
    width: 300,
    borderRadius: 8,
    paddingHorizontal: 25,
    marginBottom: 10,
  },
});

export default Input;
