//import liraries
import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

// create a component
const Button = ({text, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.Button}>
      <Text style={styles.Text}>{text}</Text>
    </TouchableOpacity>
  );
};

// define your styles
const styles = StyleSheet.create({
  Button: {
    height: 50,
    width: 150,
    backgroundColor: '#4267CD',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    marginTop: 10,
  },
  Text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});

//make this component available to the app
export default Button;
