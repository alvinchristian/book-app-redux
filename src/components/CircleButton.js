import {View, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

const CircleButton = ({Name, onPress}) => {
  return (
    <TouchableOpacity style={styles.Button} onPress={onPress}>
      <Icon name={Name} size={20} color={'#4267CD'}></Icon>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparant',
    width: 30,
    height: 30,
    borderRadius: 15,
  },
});

export default CircleButton;
