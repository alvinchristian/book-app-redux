import {View, Modal, Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {useDispatch, useSelector} from 'react-redux';
import {alertHide} from '../actions/Index';

const AlertView = () => {
  const dispatch = useDispatch();
  const Alert = useSelector(state => state.utilsData.alertStatus);
  const AlertData = useSelector(state => state.utilsData.alertData);
  return (
    <Modal visible={Alert} animationType="slide" transparent={true}>
      <View style={styles.Container}>
        <View style={styles.ModalView}>
          <View style={styles.Header}>
            <Icon
              name={AlertData.icon}
              size={60}
              color={AlertData.color}
              style={styles.Icon}></Icon>
            <Text style={{...styles.Title, color: AlertData.color}}>
              {AlertData.title}
            </Text>
          </View>
          <Text style={styles.Message}>{AlertData.message}</Text>
          <TouchableOpacity
            style={styles.Button}
            onPress={() => {
              dispatch(alertHide());
            }}>
            <Text style={styles.ButtonText}>OK</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ModalView: {
    width: '75%',
    backgroundColor: '#fff',
    height: '40%',
    borderRadius: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 25,
    paddingHorizontal: 25,
  },
  Icon: {
    marginBottom: 10,
  },
  Header: {
    borderBottomWidth: 2,
    borderColor: '#000',
    width: '100%',
    alignItems: 'center',
    paddingBottom: 10,
  },
  Title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5,
    textTransform: 'uppercase',
  },
  Message: {
    fontSize: 12,
    lineHeight: 20,
    textAlign: 'justify',
    color: '#8C96B1',
  },
  Button: {
    width: 80,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    borderWidth: 1,
  },
  ButtonText: {
    fontSize: 16,
    color: '#000',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
});

export default AlertView;
