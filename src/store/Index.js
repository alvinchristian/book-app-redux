import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import AuthReducer from '../reducers/Auth';
import BooksReducer from '../reducers/Book';
import UtilsReducer from '../reducers/Utils';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const configPersist = persistReducer(persistConfig, AuthReducer);

const Reducers = {
  authData: configPersist,
  booksData: BooksReducer,
  utilsData: UtilsReducer,
};

export const store = createStore(
  combineReducers(Reducers),
  applyMiddleware(thunk),
);
export const PersistStore = persistStore(store);
