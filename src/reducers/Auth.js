import * as T from '../types/Index';

const InitialState = {
  userData: null,
  registerStatus: false,
};

const Auth = (state = InitialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case T.GET_USER_DATA:
      return {...state, userData: payload};
      break;
    case T.REGISTER_CHECKER:
      return {...state, registerStatus: true};
      break;
    case T.CLEAR_REGISTER_STATUS:
      return {...state, registerStatus: false};
      break;
    case T.CLEAR_USER_DATA:
      return {...state, userData: null};
      break;
    default:
      return state;
  }
};

export default Auth;
