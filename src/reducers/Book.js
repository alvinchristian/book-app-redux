import * as T from '../types/Index';

const InitialState = {
  booksData: null,
  recBooksData: null,
  bookDetail: null,
};

const Book = (state = InitialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case T.GET_BOOKS:
      return {...state, booksData: payload};
      break;
    case T.GET_RECOMMENDED_BOOKS:
      return {
        ...state,
        recBooksData: payload,
      };
      break;
    case T.GET_BOOK_DETAIL:
      return {
        ...state,
        bookDetail: payload,
      };
      break;
    case T.CLEAR_BOOKS_DATA:
      return {
        ...state,
        booksData: null,
        recBooksData: null,
        bookDetail: null,
      };
      break;
    default:
      return state;
  }
};

export default Book;
