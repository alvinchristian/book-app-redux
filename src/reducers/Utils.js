import * as T from '../types/Index';

const InitialState = {
  alertStatus: false,
  alertData: {
    icon: 'eye-off',
    color: '#000',
    title: 'Title',
    message: 'Message',
  },
  connectionStatus: true,
};

const Utils = (state = InitialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case T.ALERT_SHOW:
      return {
        ...state,
        alertStatus: true,
      };
      break;
    case T.ALERT_HIDE:
      return {
        ...state,
        alertStatus: false,
      };
      break;
    case T.ALERT_DATA:
      return {
        ...state,
        alertData: payload,
      };
      break;
    case T.CONNECTED:
      return {
        ...state,
        connectionStatus: true,
      };
      break;
    case T.NOT_CONNECTED:
      return {
        ...state,
        connectionStatus: false,
      };
      break;
    default:
      return state;
  }
};

export default Utils;
