import {View, Text} from 'react-native';
import React from 'react';
import Index from './src/Index';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, PersistStore} from './src/store/Index';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={PersistStore}>
        <Index />
      </PersistGate>
    </Provider>
  );
};

export default App;
